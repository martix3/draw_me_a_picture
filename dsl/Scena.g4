//
// This is free and unencumbered software released into the public domain.
// 
// Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either
// in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and
// by any means.
// 
// For more information, please refer to <https://unlicense.org>

grammar Scena;

// Reguly parsera
scena: nazwa (NL polecenie)* NL? EOF;

nazwa: NAZWA ':';

polecenie: rysuj | napisz;

rysuj: 'narysuj' wielkosc kolor figura (x | x y);
napisz: 'napisz' NAPIS (x | x y);

wielkosc: maly | sredni | duzy;
maly: 'mały' | 'małe';
sredni: 'średni' | 'średnie';
duzy: 'duży' | 'duże';

figura: 'koło' | 'kwadrat';

kolor:
	czarny
	| niebieski
	| zielony
	| czerwony
	| pomaranczowy
	| fioletowy
	| zolty
	| bialy;

czarny: 'czarny' | 'czarne';
niebieski: 'niebieski' | 'niebieskie';
zielony: 'zielony' | 'zielone';
czerwony: 'czerwony' | 'czerwone';
pomaranczowy: 'pomarańczowy' | 'pomarańczowe';
fioletowy: 'fioletowy' | 'fioletowe';
zolty: 'żółty' | 'żółte';
bialy: 'biały' | 'białe';

x: lewo | prawo | srodek;
y: gora | dol | srodek;

lewo: 'po lewej';
prawo: 'po prawej';
srodek: 'na środku' | 'po środku';
gora: 'u góry';
dol: 'na dole';

// Reguły lexera
NAPIS: '"' .*? '"';
NAZWA: [a-zA-Z0-9\-_]+;
NL: '\r'? '\n';
SPACJA: [ \t]+ -> skip;
KOMENTARZ: '//' ~[\r\n]* -> skip;