
#ifndef SRC_MAIN
#define SRC_MAIN

#include <iostream>
#include "antlr4-runtime.h"

#include "dsl/ScenaLexer.h"
#include "dsl/ScenaParser.h"
#include "dsl/ScenaBaseVisitor.h"

#include "ElementFactory.hpp"

#include <CImg.h>

int main(int argc, const char *argv[])
{
    int exit_code = EXIT_FAILURE;

    if (argc != 2)
    {
        std::cerr << "Please specify input scene file...\n";
    }
    else
    {
        try
        {
            std::ifstream stream;
            stream.open(argv[1]);
            if (stream.fail())
            {
                std::cerr << "Could not open input scene: " << argv[1] << std::endl;
            }
            else
            {
                antlr4::ANTLRInputStream input{stream};
                dsl::ScenaLexer lexer{&input};
                antlr4::CommonTokenStream tokens{&lexer};
                dsl::ScenaParser parser{&tokens};

                const int img_size = 512;

                magu::ElementFactory factory{img_size};
                parser.scena()->accept(&factory);

                cimg_library::CImg<uint8_t> image(img_size, img_size, 1, 3, 255);

                for (auto &el : factory.elementy)
                {
                    el->draw(image);
                }

                cimg_library::CImgDisplay main_disp(image, parser.nazwa()->toString().c_str());

                main_disp.display(image);
                while (!main_disp.is_closed() && !main_disp.is_keyESC())
                {
                    main_disp.wait();
                }

                exit_code = EXIT_SUCCESS;
            }
        }
        catch (const std::exception &e)
        {
            std::cout << "Error: " << e.what() << std::endl;
        }
        catch (...)
        {
            std::cout << "Unknown error.." << std::endl;
        }
    }

    exit(exit_code);
}

#endif /* SRC_MAIN */
