/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */
#ifndef SRC_DRAWABLESCENE
#define SRC_DRAWABLESCENE

#include "dsl/ScenaParser.h"

namespace magu
{

class DrawableScene
{
public:
    DrawableScene(const dsl::ScenaParser::ScenaContext &ctx);

    void draw() const;
};

} // namespace magu

#endif /* SRC_DRAWABLESCENE */