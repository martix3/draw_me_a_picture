/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */

#include "DrawableScene.hpp"

#include <iostream>

namespace magu
{

DrawableScene::DrawableScene(const dsl::ScenaParser::ScenaContext &scena)
{
    for (auto *polecenie : scena.polecenia)
    {
        if (polecenie != nullptr)
        {
            (void)polecenie->poz_X();
            (void)polecenie->poz_Y();

            if (polecenie->figura() != nullptr)
            {
                (void)polecenie->rozmiar();

                std::cout << "Rysuje: " << polecenie->rozmiar()->getText() << ' ' << polecenie->kolor()->getText() << ' ' << polecenie->figura()->getText();
            }
            else if (polecenie->napis() != nullptr)
            {
                std::cout << "Pisze: \"" << polecenie->napis()->getText() << "\"\n";
            }
            else
            {
                throw std::runtime_error{"DrawableScene: nieprawidowe polecenie!"};
            }
        }
        else
        {
            throw std::runtime_error{"DrawableScene: polecenie == nullptr"};
        }
    }
}

void DrawableScene::draw() const
{
}

} // namespace magu