/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */

#include "ElementFactory.hpp"

namespace magu
{

ElementFactory::ElementFactory(const int imgSize)
    : mImgSize(imgSize)
{
}

antlrcpp::Any ElementFactory::visitNazwa(dsl::ScenaParser::NazwaContext *context)
{
    std::cout << "Rysuję: " << context->NAZWA()->toString() << std::endl;
    return dsl::ScenaBaseVisitor::visitChildren(context);
}

antlrcpp::Any ElementFactory::visitRysuj(dsl::ScenaParser::RysujContext *context)
{
    if (context!= nullptr && context->figura() != nullptr && context->wielkosc() != nullptr && context->x() != nullptr)
    {
        if (context->figura()->getText() == "kwadrat")
        {
            elementy.emplace_back(std::make_unique<magu::Kwadrat>(mImgSize, *context->wielkosc(), *context->kolor(), *context->x(), context->y()));
        }
        else if (context->figura()->getText() == "koło")
        {
            elementy.emplace_back(std::make_unique<magu::Kolo>(mImgSize, *context->wielkosc(), *context->kolor(), *context->x(), context->y()));
        }
    }

    return nullptr;
}

antlrcpp::Any ElementFactory::visitNapisz(dsl::ScenaParser::NapiszContext *context)
{
    if (context->NAPIS())
    {
        elementy.emplace_back(std::make_unique<magu::Napis>(mImgSize, context->NAPIS()->getText(), *context->x(), context->y()));
    }
    return nullptr;
}

} // namespace magu
