/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */

#ifndef SRC_ELEMENTY_KOLO
#define SRC_ELEMENTY_KOLO

#include "DrawableElement.hpp"

namespace magu
{

class Kolo final : public DrawableElement
{
public:
    Kolo(const int imgSize,
         dsl::ScenaParser::WielkoscContext &wielkoscCtx,
         dsl::ScenaParser::KolorContext &kolorCtx,
         dsl::ScenaParser::XContext &xCtx,
         dsl::ScenaParser::YContext *yCtx);

    void draw(cimg_library::CImg<uint8_t> &img) override;

private:
    int mRadius;
    int x;
    int y;
    RGBColor mColor;
};

} // namespace magu

#endif /* SRC_ELEMENTY_KOLO */
