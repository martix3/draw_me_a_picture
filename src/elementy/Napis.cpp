/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */

#include "Napis.hpp"

#include <CImg.h>

namespace magu
{

Napis::Napis(const int imgSize,
             const std::string napis,
             dsl::ScenaParser::XContext &xCtx,
             dsl::ScenaParser::YContext *yCtx)
    : mNapis(std::move(napis)),
      x(xPosition(imgSize, mNapis.size()*6, xCtx)),
      y(yPosition(imgSize, 12, yCtx)),
      mColor({0u, 0u, 0u})
{
}

void Napis::draw(cimg_library::CImg<uint8_t> &img)
{
    auto font = cimg_library::CImgList<uint8_t>::font(12);
    img.draw_text(x, y, mNapis.c_str(), mColor.data(), 0, 1, font);
}

} // namespace magu
