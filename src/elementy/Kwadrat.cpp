/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */

#include "Kwadrat.hpp"

#include <CImg.h>

namespace magu
{

Kwadrat::Kwadrat(const int imgSize,
                 dsl::ScenaParser::WielkoscContext &wielkoscCtx,
                 dsl::ScenaParser::KolorContext &kolorCtx,
                 dsl::ScenaParser::XContext &xCtx,
                 dsl::ScenaParser::YContext *yCtx)
    : mSize(wielkosc(imgSize, wielkoscCtx)),
      x0(xPosition(imgSize, mSize, xCtx)),
      x1(x0 + mSize),
      y0(yPosition(imgSize, mSize, yCtx)),
      y1(y0 + mSize),
      mColor(getRGBColor(kolorCtx))
{
}

void Kwadrat::draw(cimg_library::CImg<uint8_t> &img)
{
  img.draw_rectangle(x0, y0, x1, y1, mColor.data());
}

} // namespace magu