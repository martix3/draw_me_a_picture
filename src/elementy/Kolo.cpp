/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */

#include "Kolo.hpp"

#include <CImg.h>

namespace magu
{

Kolo::Kolo(const int imgSize,
           dsl::ScenaParser::WielkoscContext &wielkoscCtx,
           dsl::ScenaParser::KolorContext &kolorCtx,
           dsl::ScenaParser::XContext &xCtx,
           dsl::ScenaParser::YContext *yCtx)
    : mRadius(wielkosc(imgSize, wielkoscCtx)),
      x(xPosition(imgSize, mRadius, xCtx) + mRadius / 2),
      y(yPosition(imgSize, mRadius, yCtx) + mRadius / 2),
      mColor(getRGBColor(kolorCtx))
{
}

void Kolo::draw(cimg_library::CImg<uint8_t> &img)
{
  img.draw_circle(x, y, mRadius / 2, mColor.data(), 1);
}

} // namespace magu