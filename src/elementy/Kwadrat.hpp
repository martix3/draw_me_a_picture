/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */

#ifndef SRC_ELEMENTY_KWADRAT
#define SRC_ELEMENTY_KWADRAT

#include "DrawableElement.hpp"

namespace cimg_library
{
template <typename T>
struct CImg;
}

namespace magu
{

class Kwadrat final : public DrawableElement
{
public:
    explicit Kwadrat(const int imgSize,
                     dsl::ScenaParser::WielkoscContext &wielkoscCtx,
                     dsl::ScenaParser::KolorContext &kolorCtx,
                     dsl::ScenaParser::XContext &xCtx,
                     dsl::ScenaParser::YContext *yCtx);

    void draw(cimg_library::CImg<uint8_t> &img) override;

private:
    int mSize;
    int x0, x1;
    int y0, y1;
    RGBColor mColor;
};

} // namespace magu

#endif /* SRC_ELEMENTY_KWADRAT */
