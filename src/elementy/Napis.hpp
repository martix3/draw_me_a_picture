/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */

#ifndef SRC_ELEMENTY_NAPIS
#define SRC_ELEMENTY_NAPIS

#include "DrawableElement.hpp"

namespace magu
{

class Napis final : public DrawableElement
{
public:
    Napis(const int imgSize,
          const std::string napis,
          dsl::ScenaParser::XContext &x,
          dsl::ScenaParser::YContext *y);

    void draw(cimg_library::CImg<uint8_t> &img) override;

    private:

    const std::string mNapis;
    const int x;
    const int y;
    const RGBColor mColor;
};

} // namespace magu

#endif /* SRC_ELEMENTY_NAPIS */
