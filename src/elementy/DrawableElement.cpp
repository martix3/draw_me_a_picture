/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */

#include "elementy/DrawableElement.hpp"

namespace magu
{

int DrawableElement::wielkosc(const int sizeImg, dsl::ScenaParser::WielkoscContext &ctx)
{
    if (ctx.maly() != nullptr)
    {
        return sizeImg * 0.2;
    }
    else if (ctx.sredni() != nullptr)
    {
        return sizeImg * 0.4;
    }
    else if (ctx.duzy() != nullptr)
    {
        return sizeImg * 0.9;
    }
    else
    {
        throw std::logic_error{"DrawableElement::wielkosc(): error - Invalid WielkoscContext"};
    }
}

RGBColor DrawableElement::getRGBColor(dsl::ScenaParser::KolorContext &ctx)
{
    if (ctx.czarny() != nullptr)
    {
        return {0, 0, 0};
    }
    else if (ctx.niebieski() != nullptr)
    {
        return {0, 0, 255};
    }
    else if (ctx.zielony() != nullptr)
    {
        return {0, 255, 0};
    }
    else if (ctx.czerwony() != nullptr)
    {
        return {255, 0, 0};
    }
    else if (ctx.pomaranczowy() != nullptr)
    {
        return {255, 165, 0};
    }
    else if (ctx.fioletowy() != nullptr)
    {
        return {128, 0, 128};
    }
    else if (ctx.zolty() != nullptr)
    {
        return {255, 255, 0};
    }
    else if (ctx.bialy() != nullptr)
    {
        return {255, 255, 255};
    }
    else
    {
        throw std::invalid_argument{"DrawableElement::getRGBColor(): error - Invalid KolorContext"};
    }
}

int DrawableElement::xPosition(const int sizeImg, const int sizeElement, dsl::ScenaParser::XContext &ctx)
{
    if (ctx.lewo() != nullptr)
    {
        return sizeImg * 0.05;
    }
    else if (ctx.srodek() != nullptr)
    {
        return (sizeImg - sizeElement) * 0.5;
    }
    else if (ctx.prawo() != nullptr)
    {
        return (sizeImg * 0.95) - sizeElement;
    }
    else
    {
        throw std::invalid_argument{"DrawableElement::getRGBColor(): error - Invalid XContext"};
    }
}

int DrawableElement::yPosition(const int sizeImg, const int sizeElement, dsl::ScenaParser::YContext *ctx)
{
    if (ctx != nullptr)
    {
        if (ctx->gora() != nullptr)
        {
            return sizeImg * 0.05;
        }
        else if (ctx->srodek() != nullptr)
        {
            return (sizeImg - sizeElement) * 0.5;
        }
        else if (ctx->dol() != nullptr)
        {
            return (sizeImg * 0.95) - sizeElement;
        }
        else
        {
            throw std::invalid_argument{"DrawableElement::getRGBColor(): error - Invalid XContext"};
        }
    }
    else
    {
        return (sizeImg - sizeElement) * 0.5;
    }
    
}

} // namespace magu