/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */

#ifndef SRC_DRAWABLEELEMENT
#define SRC_DRAWABLEELEMENT

#include "dsl/ScenaParser.h"
#include "CImg.h"

namespace magu
{

using RGBColor = std::array<uint8_t, 3>;

class DrawableElement
{
public:
    DrawableElement() = default;

    virtual ~DrawableElement() = default;

    virtual void draw(cimg_library::CImg<uint8_t> &img) = 0;

protected:
    static int wielkosc(const int sizeImg, dsl::ScenaParser::WielkoscContext &ctx);

    static RGBColor getRGBColor(dsl::ScenaParser::KolorContext &ctx);

    static int xPosition(const int sizeImg, const int sizeElement, dsl::ScenaParser::XContext &ctx);

    static int yPosition(const int sizeImg, const int sizeElement, dsl::ScenaParser::YContext *ctx);
};

} // namespace magu

#endif /* SRC_DRAWABLEELEMENT */
