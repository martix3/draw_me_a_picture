/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * For more information, please refer to <https://unlicense.org>
 */

#ifndef SRC_ELEMENTFACTORY
#define SRC_ELEMENTFACTORY

#include "dsl/ScenaBaseVisitor.h"

#include "elementy/Kolo.hpp"
#include "elementy/Kwadrat.hpp"
#include "elementy/Napis.hpp"

namespace magu
{

class ElementFactory : public dsl::ScenaBaseVisitor
{
public:
    explicit ElementFactory(const int imgSize);

    antlrcpp::Any visitNazwa(dsl::ScenaParser::NazwaContext *context) override;

    antlrcpp::Any visitRysuj(dsl::ScenaParser::RysujContext *context) override;

    antlrcpp::Any visitNapisz(dsl::ScenaParser::NapiszContext *context) override;

    std::vector<std::unique_ptr<magu::DrawableElement>> elementy;

private:
    const int mImgSize;
};

} // namespace magu

#endif /* SRC_ELEMENTFACTORY */
