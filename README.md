# draw_me_a_picture

## Build

### Requirements
- C++14 compiler
- ANTLR 4.8 generator (installation script in ./script)
- Meson 
- Ninja
- CMake 3.x
- python 3.x
- X11 libraries 

For Ubuntu:
`sudo apt install python3 python3-pip cmake ninja-build libx11-dev`
`sudo pip3 install meson`
`./script/install_antlr4cpp.sh`

### Build steps

1. `$ meson <build_dir>`
2. `$ ninja -C <build_dir>`

### Usage

`$ ./draw_me_a_picture <scene_file>`

e.g.

`$ ./build/draw_me_a_picture test/test.scene`
