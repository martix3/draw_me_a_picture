#
# This is free and unencumbered software released into the public domain.
# 
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
# 
# For more information, please refer to <https://unlicense.org>
# 

#!/bin/sh

antlr_version="4.8"

if ls /usr/local/lib/antlr-* > /dev/null 2>&1;
then
    echo "antlr already installed, removing prevouis version"
    sudo rm /usr/local/lib/antlr* -f
    sudo rm /usr/local/bin/antlr* -f
    sed -i '/export CLASSPATH=\/usr\/local\/lib\/antlr-*/d' $HOME/.bashrc
fi

cd /tmp
if ! ls /tmp/antlr-$antlr_version-complete.jar > /dev/null 2>&1;
then
    wget http://www.antlr.org/download/antlr-$antlr_version-complete.jar
fi

if ! ls /tmp/antlr4-cpp-runtime-$antlr_version-source.zip > /dev/null 2>&1;
then
    wget https://www.antlr.org/download/antlr4-cpp-runtime-$antlr_version-source.zip
fi


sudo cp antlr-$antlr_version-complete.jar /usr/local/lib
echo "export CLASSPATH=/usr/local/lib/antlr-$antlr_version-complete.jar:\$CLASSPATH" >> $HOME/.bashrc

sudo touch /usr/local/bin/antlr4
sudo chmod a+xw /usr/local/bin/antlr4

echo "#!/bin/sh" > /usr/local/bin/antlr4
echo "java -jar /usr/local/lib/antlr-$antlr_version-complete.jar \"\$@\"" >> /usr/local/bin/antlr4
source $HOME/.bashrc

sudo pip3 install meson
sudo apt install -y ninja-build uuid-dev



cd -
